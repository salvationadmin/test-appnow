
function loadData() {

    var $body = $('body');
    var $wikiElem = $('#wikipedia-links');
    var $nytHeaderElem = $('#nytimes-header');
    var $nytElem = $('#nytimes-articles');
    var $greeting = $('#greeting');

    // clear out old data before new request
    $wikiElem.text("");
    $nytElem.text("");

    // load streetview

    // YOUR CODE GOES HERE!
    ko.bindingHandlers.googlemap = {
    init: function (element, valueAccessor) {
        var
          value = valueAccessor(),
          latLng = new google.maps.LatLng(value.latitude, value.longitude),
          mapOptions = {
            zoom: 10,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            },
          map = new google.maps.Map(element, mapOptions),
          marker = new google.maps.Marker({
            position: latLng,
            map: map
          });
    }
};

var vm =  {
    locations: ko.observableArray([
        {name: "Cleveland", latitude:41.48 , longitude:-81.67},
        {name: "Chicago", latitude: 41.88, longitude: -87.63}
    ])
}
    
ko.applyBindings(vm);



    return false;
};

$('#form-container').submit(loadData);

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  // *
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  'html': {
    'fontFamily': ''Roboto', 'Helvetica', sans-serif',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    // height implementation to add full screen google maps
    'height': [{ 'unit': '%V', 'value': 1 }]
  },
  'body': {
    'fontFamily': ''Roboto', 'Helvetica', sans-serif',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    // height implementation to add full screen google maps
    'height': [{ 'unit': '%V', 'value': 1 }]
  },
  'container': {
    'height': [{ 'unit': 'string', 'value': 'inherit' }]
  },
  'container>div': {
    'height': [{ 'unit': 'string', 'value': 'inherit' }]
  },
  'container>div #map-canvas': {
    'height': [{ 'unit': 'string', 'value': 'inherit' }]
  },
  '*': {
    'boxSizing': 'border-box'
  },
  'body': {
    'backgroundColor': '#CCC'
  },
  'map': {
    'height': [{ 'unit': 'px', 'value': 200 }],
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'bgimg': {
    'position': 'fixed',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'zIndex': '-1',
    'opacity': '1'
  },
  'greeting': {
    'color': 'black',
    'textShadow': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'string', 'value': 'rgba(255,255,255, .5)' }],
    'textAlign': 'center'
  },
  'wikipedia-container': {
    'float': 'right',
    'width': [{ 'unit': 'px', 'value': 290 }],
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }],
    'marginRight': [{ 'unit': 'px', 'value': 10 }],
    'backgroundColor': 'rgba(255,255,255,0.7)'
  },
  'nytimes-container': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }],
    'marginRight': [{ 'unit': 'px', 'value': 300 }],
    'backgroundColor': 'rgba(255,255,255,0.7)'
  },
  'article-list': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'article': {
    'listStyleType': 'none',
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }],
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#CCC' }]
  },
  'article:first-child': {
    'borderTop': [{ 'unit': 'string', 'value': 'none' }]
  },
  'mdl-demo mdl-layout__header-row': {
    'paddingLeft': [{ 'unit': 'px', 'value': 40 }]
  },
  'mdl-demo mdl-layoutis-small-screen mdl-layout__header-row h3': {
    'fontSize': [{ 'unit': 'string', 'value': 'inherit' }]
  },
  'mdl-demo mdl-layout__tab-bar-button': {
    'display': 'none'
  },
  'mdl-demo mdl-layoutis-small-screen mdl-layout__tab-bar mdl-button': {
    'display': 'none'
  },
  'mdl-demo mdl-layout:not(is-small-screen) mdl-layout__tab-bar': {
    'overflow': 'visible'
  },
  'mdl-demo mdl-layout:not(is-small-screen) mdl-layout__tab-bar-container': {
    'overflow': 'visible'
  },
  'mdl-demo mdl-layout__tab-bar-container': {
    'height': [{ 'unit': 'px', 'value': 64 }]
  },
  'mdl-demo mdl-layout__tab-bar': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 16 }],
    'boxSizing': 'border-box',
    'height': [{ 'unit': '%V', 'value': 1 }],
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'mdl-demo mdl-layout__tab-bar mdl-layout__tab': {
    'height': [{ 'unit': 'px', 'value': 64 }],
    'lineHeight': [{ 'unit': 'px', 'value': 64 }]
  },
  'mdl-demo mdl-layout__tab-bar mdl-layout__tabis-active::after': {
    'backgroundColor': 'white',
    'height': [{ 'unit': 'px', 'value': 4 }]
  },
  'mdl-demo main > mdl-layout__tab-panel': {
    'padding': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }],
    'paddingTop': [{ 'unit': 'px', 'value': 48 }]
  },
  'mdl-demo mdl-card': {
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'display': '-webkit-flex',
    'display': '-ms-flexbox',
    'display': 'flex',
    'WebkitFlexDirection': 'column',
    'MsFlexDirection': 'column',
    'flexDirection': 'column'
  },
  'mdl-demo mdl-card > *': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'mdl-demo mdl-card mdl-card__supporting-text': {
    'margin': [{ 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 40 }],
    'WebkitFlexGrow': '1',
    'MsFlexPositive': '1',
    'flexGrow': '1',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'color': 'inherit',
    'width': [{ 'unit': '%H', 'value': NaN }]
  },
  'mdl-demomdl-demo mdl-card__supporting-text h4': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': 20 }]
  },
  'mdl-demo mdl-card__actions': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 40 }],
    'color': 'inherit'
  },
  'mdl-demo mdl-card__actions a': {
    'color': '#00BCD4',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'mdl-demo mdl-card__actions a:hover': {
    'color': 'inherit',
    'backgroundColor': 'transparent'
  },
  'mdl-demo mdl-card__actions a:active': {
    'color': 'inherit',
    'backgroundColor': 'transparent'
  },
  'mdl-demo mdl-card__supporting-text + mdl-card__actions': {
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, 0.12)' }]
  },
  'mdl-demo #add': {
    'position': 'absolute',
    'right': [{ 'unit': 'px', 'value': 40 }],
    'top': [{ 'unit': 'px', 'value': 36 }],
    'zIndex': '999'
  },
  'mdl-demo mdl-layout__content section:not(:last-of-type)': {
    'position': 'relative',
    'marginBottom': [{ 'unit': 'px', 'value': 48 }]
  },
  'mdl-demo sectionsection--center': {
    'maxWidth': [{ 'unit': 'px', 'value': 860 }]
  },
  'mdl-demo #features sectionsection--center': {
    'maxWidth': [{ 'unit': 'px', 'value': 620 }]
  },
  'mdl-demo section > header': {
    'display': '-webkit-flex',
    'display': '-ms-flexbox',
    'display': 'flex',
    'WebkitAlignItems': 'center',
    'MsFlexAlign': 'center',
    'alignItems': 'center',
    'WebkitJustifyContent': 'center',
    'MsFlexPack': 'center',
    'justifyContent': 'center'
  },
  'mdl-demo section > section__play-btn': {
    'minHeight': [{ 'unit': 'px', 'value': 200 }]
  },
  'mdl-demo section > header > material-icons': {
    'fontSize': [{ 'unit': 'rem', 'value': 3 }]
  },
  'mdl-demo section > button': {
    'position': 'absolute',
    'zIndex': '99',
    'top': [{ 'unit': 'px', 'value': 8 }],
    'right': [{ 'unit': 'px', 'value': 8 }]
  },
  'mdl-demo section section__circle': {
    'display': '-webkit-flex',
    'display': '-ms-flexbox',
    'display': 'flex',
    'WebkitAlignItems': 'center',
    'MsFlexAlign': 'center',
    'alignItems': 'center',
    'WebkitJustifyContent': 'flex-start',
    'MsFlexPack': 'start',
    'justifyContent': 'flex-start',
    'WebkitFlexGrow': '0',
    'MsFlexPositive': '0',
    'flexGrow': '0',
    'WebkitFlexShrink': '1',
    'MsFlexNegative': '1',
    'flexShrink': '1'
  },
  'mdl-demo section section__text': {
    'WebkitFlexGrow': '1',
    'MsFlexPositive': '1',
    'flexGrow': '1',
    'WebkitFlexShrink': '0',
    'MsFlexNegative': '0',
    'flexShrink': '0',
    'paddingTop': [{ 'unit': 'px', 'value': 8 }]
  },
  'mdl-demo section section__text h5': {
    'fontSize': [{ 'unit': 'string', 'value': 'inherit' }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'em', 'value': 0.5 }]
  },
  'mdl-demo section section__text a': {
    'textDecoration': 'none'
  },
  'mdl-demo section section__circle-container > section__circle-container__circle': {
    'width': [{ 'unit': 'px', 'value': 64 }],
    'height': [{ 'unit': 'px', 'value': 64 }],
    'borderRadius': '32px',
    'margin': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 0 }]
  },
  'mdl-demo sectionsection--footer section__circle--big': {
    'width': [{ 'unit': 'px', 'value': 100 }],
    'height': [{ 'unit': 'px', 'value': 100 }],
    'borderRadius': '50px',
    'margin': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 32 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 32 }]
  },
  'mdl-demo is-small-screen sectionsection--footer section__circle--big': {
    'width': [{ 'unit': 'px', 'value': 50 }],
    'height': [{ 'unit': 'px', 'value': 50 }],
    'borderRadius': '25px',
    'margin': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 16 }]
  },
  'mdl-demo sectionsection--footer': {
    'padding': [{ 'unit': 'px', 'value': 64 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 64 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': -8 }, { 'unit': 'px', 'value': -8 }, { 'unit': 'px', 'value': -8 }]
  },
  'mdl-demo sectionsection--center section__text:not(:last-child)': {
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(0,0,0,.13)' }]
  },
  'mdl-demo mdl-card mdl-card__supporting-text > h3:first-child': {
    'marginBottom': [{ 'unit': 'px', 'value': 24 }]
  },
  'mdl-demo mdl-layout__tab-panel:not(#overview)': {
    'backgroundColor': 'white'
  },
  'mdl-demo #features section': {
    'marginBottom': [{ 'unit': 'px', 'value': 72 }]
  },
  'mdl-demo #features h4': {
    'marginBottom': [{ 'unit': 'px', 'value': 16 }]
  },
  '#features h5': {
    'marginBottom': [{ 'unit': 'px', 'value': 16 }]
  },
  'mdl-demo toc': {
    'borderLeft': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#C1EEF4' }],
    'margin': [{ 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 24 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 8 }],
    'display': '-webkit-flex',
    'display': '-ms-flexbox',
    'display': 'flex',
    'WebkitFlexDirection': 'column',
    'MsFlexDirection': 'column',
    'flexDirection': 'column'
  },
  'mdl-demo toc h4': {
    'fontSize': [{ 'unit': 'rem', 'value': 0.9 }],
    'marginTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'mdl-demo toc a': {
    'color': '#4DD0E1',
    'textDecoration': 'none',
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'lineHeight': [{ 'unit': 'px', 'value': 28 }],
    'display': 'block'
  },
  'mdl-demo mdl-menu__container': {
    'zIndex': '99'
  }
});
